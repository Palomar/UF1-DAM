"""
5. Algorisme que llegeix 3 nombres i diu quin és el major. 
"""
NUM1 = input("Entra el número 1: ")
NUM2 = input("Entra el número 2: ")
NUM3 = input("Entra el número 3: ")
if NUM1 > NUM2 and NUM1 > NUM3:
    print("El número 1, " + NUM1 + " és el més gran.")
elif NUM2 > NUM1 and NUM2 > NUM3:
    print("El número 2, " + NUM2 + " és el més gran.")
elif NUM3 > NUM1 and NUM3 > NUM2:
    print("El número 3, " + NUM3 + " és el més gran.")
elif NUM1 == NUM2 and NUM1 == NUM3:
    print("Són iguals.")
