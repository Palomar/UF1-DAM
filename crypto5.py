"""
Codifica un fitxer en AES
"""
from Crypto.Cipher import AES

FITXER_IN = open("logo.svg", "rb")
FITXER_OUT = open("logo.cod", "wb")
CLAU = "1234567812345678".encode('UTF-8')
IV = "1234567812345678".encode('UTF-8')
OBJ1 = AES.new(CLAU, AES.MODE_CBC, IV)

while True:
    BLOC = FITXER_IN.read(8192)
    if not BLOC:
        break
    LONGITUD = len(BLOC)
    RESIDU = LONGITUD % 16
    if RESIDU > 0:
        PADDING = 16 - RESIDU
        BLOC += b'0' * PADDING
    COD = OBJ1.encrypt(BLOC)
    FITXER_OUT.write(COD)
FITXER_IN.close()
FITXER_OUT.close()
