"""
13. Algorisme que llegeix 3 valors corresponents als coeficients
A, B, i C d'una equació de segon grau i
ens diu la solució aplicant la fórmula de resolució.
"""
import math;
A = float(input("Introdueix valor A: "))
B = float(input("Introdueix valor B: "))
C = float(input("Introdueix valor C: "))
#calcula equació segon grau
X = (B*B) - (4*A*C)
if X < 0:
    print("El discriminant és negatiu, per tant és imaginari.")
elif X == 0:
    X = -B / (2*A)
    print("El resultat de X és: " + str(X) + ".")
elif X > 0:
    X = ((-B)+(math.sqrt((B*B) - (4*A*C)))) / (2*A)
    print("El resultat de X és: " + str(X) + ".")
    X = ((-B)+(math.sqrt((B*B) - (4*A*C)))) / (2*A)
    print("O el resultat de X és: " + str(X) + ".")
