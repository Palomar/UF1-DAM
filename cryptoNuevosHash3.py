"""
Guarda dades usuari en fitxer
"""
from Crypto.Hash import SHA256
IDENTIFICADOR = input("Introdueix identificador: ")
NOM = input("Introdueix nom: ")
CLAU = input("Introdueix la clau: ")
OBJ = SHA256.new(CLAU.encode('UTF-8'))
RESUM = OBJ.hexdigest()
FITXER = open("fitxer.txt", 'w')
FITXER.write(IDENTIFICADOR + ":" + NOM + ":" + RESUM)
FITXER.close()
